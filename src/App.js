import React from 'react';
import logo from './logo.svg';
import CanvasForm from './Components/CanvasForm.js'
import EmployeeTable from './Components/EmployeeTable.js'
import { withRouter } from "react-router-dom";
import {Section, Container, MediaContent, Media,
 Checkbox, Field, Title, Subtitle, Columns, Column,
Button, Table, Footer , Content, Icon, Box}from 'bloomer';

import UserService from "./api/UserService.js";

class App  extends React.Component {

 constructor(props) {
    super(props);
    this.state  = { 
      employee:{
        id:null, 
        name:"",
        quantity:400, 
        dayLabored: 2, 
        name:"",
        rfc:"",
        nss:"",
        curp:"",
        nss:"",
        antiquity:"",
        rol:""
      }
   };
  
  }

  componentDidMount(){
      
   const id = this.props.match.params.id;

    
  UserService.getCalculation(id).then(result => {
      let data= result[0];
      
      console.log(data);
        this.setState({ 
          employee:{
            id:data.id,
            name: data.name,
            rfc: data.rfc,
            nss:data.nss,
            curp:data.curp,
            antiquity:data.antiquity,
            rol: data.rol
          }
        });
      
    })
    .catch(err => {
        console.error(err);
    });
  }


  render()  {
    
    return (
      
        <Container>
         <Title> Sistema de nóminas mirador 600</Title>
         <Subtitle>{this.state.employee.Name}</Subtitle>
         <EmployeeTable employee={this.state.employee}/>
         <CanvasForm />
         </Container>
      
    );
  }
}

export default withRouter(App);

