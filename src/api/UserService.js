var params = { method: 'GET'};

const urlService='http://localhost:8000/';
function getUsers(callBack){
	return fetch(urlService.concat('api/users'), {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':'localhost:3000'
        }
    })
    .then(function(response) {
        console.log('response =', response);
        return response.json();
    })
    .catch(function(err) {
        console.error(err);
    });
	
}

function transformToJson(response){
	 console.log('response =', response);
	 return response.json();
}

function getCalculation(id){
	return fetch(urlService.concat(`api/calculation/${id}`))
    .then(transformToJson);
}
const UserService  ={getCalculation, getUsers};
export  default UserService ;
