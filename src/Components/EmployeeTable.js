import React from 'react';
import  "./canvas.css";
import Buttons from './Buttons.js'

import {Table, Button}from 'bloomer';


export default  class  EmployeeTable extends React.Component {


  constructor(props) {
    super(props);
   
  
  }

  render() {
  	const employee = this.props.employee;
    return (
    	<Table isBordered isStriped isNarrow className="is-hoverable">
        <thead>
            <tr className="has-background-primary">
                <th>NO. de Empleado</th>
                <th>Nombre de Empleado</th>
                <th>RFC</th>
                <th>NSS</th>
                
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{employee.id}</td>
                <td>{employee.name}</td>
                <td>{employee.rfc}</td>
                <td>{employee.nss}</td>
            </tr>
            
            <tr >
                <th>CURP</th>
                <th>Antiguedad</th>
                <th colSpan="2">DEPARTAMENTO</th>
            </tr>
            
             <tr>
                <td>{employee.curp}</td>
                <td>{employee.antiquity}</td>
                <td colSpan="2">{employee.rol}</td>
                
            </tr>
        </tbody>
    </Table>

    );
  }
}


