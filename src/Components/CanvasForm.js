import React from 'react';
import  "./canvas.css";
import Buttons from './Buttons.js'
import {Section, Container, MediaContent, Media,
 Checkbox, Field, Title, Subtitle, Columns, Column,
Button, Footer , Content, Icon, Box}from 'bloomer';


export default  class  CanvasForm extends React.Component {

handlePolitics(event){
	
	this.canvas.classList.toggle("disableCanvas");
	if(!event.target.checked){
		this.clearCanvas();
		this.setState({politics:false});
	}else{
		this.setState({politics:true});
	}
}

handleSubmit(event){
	this.setState({ sign:this.canvas.toDataURL()})
}

clearCanvas(){
		  this.canvas.width =  this.canvas.width;
		 console.log("Clearing canvas");

}
componentDidMount() {
 		var canvas;
    	var drawing;
		var mousePos;
		var lastPos;
		var clearEl
		var ctx;

		canvas = this.canvas;
		ctx = canvas.getContext("2d");

	  	drawing = false;
		mousePos = { x:0, y:0 };
		lastPos= mousePos;
		

		var requestAnimFrame = (function (callback) {
		        return window.requestAnimationFrame || 
		           window.webkitRequestAnimationFrame ||
		           window.mozRequestAnimationFrame ||
		           window.oRequestAnimationFrame ||
		           window.msRequestAnimaitonFrame ||
		           function (callback) {
		        window.setTimeout(callback, 1000/60);
		           };
		})();

		function renderCanvas(){
			if(drawing){
				ctx.moveTo(lastPos.x,lastPos.y );
				ctx.lineTo(mousePos.x, mousePos.y);
				ctx.stroke();
				lastPos=mousePos;
			}
		}


		(function drawLoop () {
		  requestAnimFrame(drawLoop);
		  renderCanvas();
		})();

		canvas.addEventListener("mousedown", (e)=>{
			console.log("Hi");
			drawing=true;
			 lastPos = getMousePos(canvas, e);
		},false);

		canvas.addEventListener("mouseup", (e)=>{
			drawing=false;
		},false);

		canvas.addEventListener("mousemove", (e)=>{
			mousePos = getMousePos(canvas, e);
		},false);


		canvas.addEventListener("touchstart", function (e) {
		        mousePos = getTouchPos(canvas, e);
		  var touch = e.touches[0];
		  var mouseEvent = new MouseEvent("mousedown", {
		    clientX: touch.clientX,
		    clientY: touch.clientY
		  });
		  canvas.dispatchEvent(mouseEvent);
		}, false);
		canvas.addEventListener("touchend", function (e) {
		  var mouseEvent = new MouseEvent("mouseup", {});
		  canvas.dispatchEvent(mouseEvent);
		}, false);
		canvas.addEventListener("touchmove", function (e) {
		  var touch = e.touches[0];
		  var mouseEvent = new MouseEvent("mousemove", {
		    clientX: touch.clientX,
		    clientY: touch.clientY
		  });
		  canvas.dispatchEvent(mouseEvent);
		}, false);

		function getMousePos (canvasDom, mouseEvent){
		  var rect = canvasDom.getBoundingClientRect();
		  return {
		    x: mouseEvent.clientX - rect.left,
		    y: mouseEvent.clientY - rect.top
		  };
		}

		function getTouchPos(canvasDom, touchEvent) {
		  var rect = canvasDom.getBoundingClientRect();
		  return {
		    x: touchEvent.touches[0].clientX - rect.left,
		    y: touchEvent.touches[0].clientY - rect.top
		  };
		}

	
  	}


  componentWillUnmount() {



  }

  constructor(props) {
    super(props);
    this.state = {
	   politics:false,      
       sign: null
    };
  
  }

  render() {
    return (
      <Media>
         <MediaContent>
             <Field>
                <Checkbox onClick={(i)=> this.handlePolitics(i)}>Entiendo las politicas de privacidad </Checkbox>

            </Field>
            <Field>
             
		        <canvas  className="disableCanvas"ref={(c) => {this.canvas= c;}} >Not supported in current browser, please install firefox or google chrome</canvas>
		      	

              
            </Field>
              <Buttons>
                <Button isColor="default" onClick={(i)=>this.clearCanvas(i)}>Limpiar</Button>

                 {this.state.politics  &&
		        	<Button isColor="success" onClick={(i)=> this.handleSubmit(i)}>Enviar</Button>
		      	}
		      	 {!this.state.politics  &&
		        	<Button isColor="success" disabled>Enviar</Button>
		      	}
		      	
                
              </Buttons>
           </MediaContent>
        </Media>
    );
  }
}


