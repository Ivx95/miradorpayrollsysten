import React from 'react';
import {Footer, Container, Columns, Column,
   Content, Icon, Box}from 'bloomer';


export default class MyFooter extends React.Component{


  constructor(props) {
    super(props);
  }
  
 render() {
 	return(
	 <Footer id='footer'>
        <Container>
            <Content>
                <Columns>
                    <Column isfull="true">
                        <p>
                            Made with<Icon hasTextColor="danger" className="fa fa-heart"></Icon> 
                            by <a>Jaisen Ivan</a>
                        </p>
                    </Column>
                </Columns>
                <Content isSize='small'>
                    <p>The source code is licensed under <a target="_blank">MIT</a>.</p>
                    <p>The website content is licensed under <a target="_blank">CC ANS 4.0</a>.</p>
                </Content>
            </Content>
        </Container>
      </Footer>  )
  }

}