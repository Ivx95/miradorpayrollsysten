import { Link } from 'react-router-dom';
import React from 'react';
import logo from '../logo.svg';
import {Navbar, NavbarBrand, NavbarItem, NavbarMenu,
		NavbarStart, NavbarDropdown, NavbarDivider,
		Icon, Button,
		NavbarLink, NavbarEnd, Control, Field}from 'bloomer';

/**
*/
function NavbarCustom(props){
	return(
		<Navbar>
			<NavbarBrand>
				 <NavbarItem>
        			<img src={logo}  style={{ marginRight: 5 }} /> Mirador 600
   				 </NavbarItem>
			</NavbarBrand>
			<NavbarMenu >
			    <NavbarStart>
			        <NavbarItem href='#/'>Home</NavbarItem>
			        <NavbarItem hasDropdown isHoverable>
			            <NavbarLink href='#/documentation'></NavbarLink>
			            <NavbarDropdown>
			                <NavbarItem> <Link to="/app/1" >App</Link></NavbarItem>
			            </NavbarDropdown>
			        </NavbarItem>
			    </NavbarStart>
			    
			</NavbarMenu>
		</Navbar>
		);

}

export default NavbarCustom;