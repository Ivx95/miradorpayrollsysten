import React from 'react';
import {Message, MessageHeader, Delete, MessageBody }from 'bloomer';
import { Link } from 'react-router-dom';
import {Table, Button,Section, Container}from 'bloomer';
import App from '../App.js';
import {
  Switch,
  Route,
  useRouteMatch,withRouter,
  Redirect
} from "react-router-dom";

import UserService from "../api/UserService";




class Employees extends React.Component{


	handleClick(value){
		//console.log(value);
		const match = this.props.match;
		this.props.history.push("app/"+value);

		//this.setState({emplSelected:true, empId:value});
	}

	constructor(props) {
    	super(props);
    	this.state  = { users:[]
    	}
  	}


  	componentDidMount(){
  		UserService.getUsers().then((users)=>{
  			this.setState({users:users});	
  		});
  		
  	}


	render(){


			const items = this.state.users.map((item, key) =>
		             	<tr key={item.id} onClick={()=>this.handleClick(item.id)}> 
		                	<td>	
			                	{item.id}
		                	</td>
		                	<td>{item.name}</td>
		                </tr>
	    	);
			const path =this.props.match.path;

			return (
		    <Section>
		        <Container>
			    	<Table isBordered isStriped isNarrow className="is-hoverable">
				        <thead>
				            <tr className="has-background-primary">
				                <th>NO. de Empleado</th>
				                <th>Nombre de Empleado</th>
				            </tr>
				        </thead>
				        <tbody>
				           {items}
				        </tbody>
				    </Table>
			    </Container>
		    </Section>
		    );

	}
}


export default withRouter(Employees);