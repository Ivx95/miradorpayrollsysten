
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import NavbarCustom from './Navbar.js'
import Home2 from './Home2.js'
import MyFooter from './Footer.js'
import App from '../App.js'
import Employees from './Employees.js'
import ErrorPage from './ErrorPage.js'
import {Section } from "bloomer"

export default class RouterCustom extends Component {
  render() {
    return (
      <BrowserRouter>
        <Section>
          <NavbarCustom />
          <Redirect
            from="/"
            to="/home" />
          <Switch>
            <Route
              path="/employees"
              component={Employees} />
            <Route path={`/app/:id`}>
               <App />
            </Route>

              <Route
              path="/home"
              component={Home2} />
            
            <Route component={ErrorPage} />
          </Switch>
          <MyFooter/>
        </Section>
      </BrowserRouter>
    );
  }
}